import React, { Component } from 'react'
import { View, TouchableOpacity, Text, FlatList, Image, Dimensions, StyleSheet, ScrollView, Animated } from 'react-native'
import { Header, Icon, Card } from 'react-native-elements';
import Axios from 'axios';

export default class subscribe extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            activeSections: [],
            refreshing: false,
        }
    }

    onLoadData = () => {
        this.setState({ refreshing: true })
        Axios.get("http://bk-id.herokuapp.com/users")
            .then((res) => {
                if (res.status == 200) {
                    let response = []
                    res.data.map((item, index) => {
                        if (!item.name && !item.studentID) {
                            response.push(item)
                        }
                    })
                    this.setState({ data: response, refreshing: false })
                }
            })
    }

    componentDidMount = () => {
        this.onLoadData()
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity>
                <Card style={{ width: '100', backgroundColor: 'white', flexDirection: 'column' }}>
                    <Text>Dataaa</Text>
                </Card>
            </TouchableOpacity>
        )
    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
                    <Icon
                        name='menu'
                        color='white'
                    />
                </TouchableOpacity>
            </View>
        );
    }

    renderRight = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('services')}>
                    <Image style={{ width: 30, height: 30, tintColor: 'white' }} source={require('./assets/plus.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={s.title}>SERVICES</Text>
            </View>
        );
    }


    render() {
        const { data } = this.state
        return (
            <View style={{ flex: 1 }}>
                <Header
                    leftComponent={this.renderMenu()}
                    centerComponent={this.renderCenter()}
                    rightComponent={this.renderRight()}
                    outerContainerStyles={{ backgroundColor: '#445870' }}
                />
                <ScrollView>
                    <FlatList
                        data={data}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item._id}
                        ListEmptyComponent={
                            <View style={{ marginTop: 10 }}>
                                <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 15 }}>Currently you didn't subscribe any service</Text>
                            </View>
                        }
                    />

                </ScrollView>
            </View>
        );
    }
}

const s = StyleSheet.create({
    Button: {
        width: 200,
        height: 50,
        backgroundColor: '#47525E',
        borderRadius: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom: 30,
    },
    items: {
        backgroundColor: 'green',
        borderRadius: 10,
        width: '70%',
        height: Dimensions.get('window').height * 0.1,
        alignContent: 'center',
        justifyContent: 'center', alignSelf: 'center',
        marginLeft: 10
    },
    imageItem: {
        height: Dimensions.get('window').height * 0.15,
        width: Dimensions.get('window').width * 0.25,
        resizeMode: 'contain'
    },
    buttonTitle: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 15
    },
    title: {
        marginLeft: 10,
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white'
    }
});
