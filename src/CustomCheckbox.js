import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { CheckBox } from 'react-native-elements';

export default class CustomCheckbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
  }
  toggleChange() {
    this.setState({ checked: !this.state.checked });
  }
  render() {
    return (
      <View style={{ flexDirection: 'row', backgroundColor: '#0099ff', justifyContent: 'center',alignItems: 'center', }}>
        <CheckBox
          checked={this.props.checked}
          title={this.props.name}
          checkedIcon='dot-circle-o'
          uncheckedIcon='circle-o'
          // onPress={() => this.toggleChange()}
          containerStyle={{ backgroundColor: '#fff', width: '95%', borderWidth: 0 ,marginRight:5}}
          size={10}
        />
      </View>
    );
  }
}