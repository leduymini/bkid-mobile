import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, ImageBackground, Dimensions } from 'react-native';
import { Header, Icon, Card } from 'react-native-elements';
import { LineChart } from 'react-native-chart-kit';

export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
                    <Icon
                        color='white'
                        name='menu' />
                </TouchableOpacity>
            </View>
        );
    }
    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={s.title}>HOME</Text>
            </View>
        );
    }


    render() {
        const data = {
            labels: ['Park B10', 'Park B4', 'Food C6', 'Food B4', 'Lab C5'],
            datasets: [{
                data: [20, 45, 28, 51, 60]
            }]
        }

        const student = this.props.navigation.getParam('api')
        return (
            <View style={{ flex: 1 }}>
                <Header
                    leftComponent={this.renderMenu()}
                    centerComponent={this.renderCenter()}
                    outerContainerStyles={{ backgroundColor: '#50CBC2' }}
                />
                <ScrollView>
                    <View style={{ backgroundColor: '#f8f8f9', marginBottom: 50 }}>
                        <Card containerStyle={{ backgroundColor: '#5F86D2', elevation: 1, borderRadius: 10 }}>
                            {/* <View style={{marginTop: 30, backgroundColor:'#5bb5ff'}}> */}
                            <TouchableOpacity style={{ justifyContent: 'center', justifyContent: 'space-between', flexDirection: "row" }} onPress={() => this.props.navigation.navigate('User details', { student: student })}>
                                <Image source={require('./assets/account.png')} style={{ marginLeft: 10, width: 70, height: 70 }} resizeMode="contain" />
                                <View style={{ justifyContent: 'center', marginRight: 10 }}>
                                    <Text style={{ fontWeight: 'bold', fontStyle: 'italic', fontSize: 20 }}>Welcome back,</Text>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', fontSize: 30, color: '#f7f7f8' }}>{student.name}</Text>
                                </View>
                                {/* <Text style={{ color: '#f7f7f8' }}>{student.email}</Text>
                                <Text style={{ color: '#f7f7f8' }}>{student.phone}</Text> */}
                                {/* <View style={s.more}>
                                    <Text style={{ fontWeight: 'bold' }}>More details ></Text>
                                </View> */}
                            </TouchableOpacity>
                            {/* </View> */}
                        </Card>

                        <View style={{ flexDirection: 'row',marginTop:10 }}>
                            <View style={{ backgroundColor: 'black', height: 2, flex: 1, alignSelf: 'center' }} />
                            <Text style={{ alignSelf: 'center', paddingHorizontal: 5, fontSize: 30 }}>Coming soon</Text>
                            <View style={{ backgroundColor: 'black', height: 2, flex: 1, alignSelf: 'center' }} />
                        </View>

                        <Card containerStyle={{ backgroundColor: '#2052B5', borderRadius: 10, justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fff' }}>BALANCE</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#fff' }}>Coming soon</Text>
                                </View>

                            </TouchableOpacity>
                        </Card>
                        {/* <Card containerStyle={{ borderRadius: 10, backgroundColor: '#83E6B4' }}> */}
                        <View>

                            {/* <TouchableOpacity> */}
                            <View style={{ flexDirection: 'row', justifyContent: "center", width: '100%', marginTop: 20 }}>
                                <Image source={require('./assets/overview_chart.jpg')} style={{ width: 40, height: 40 }} resizeMode={'contain'} />
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', marginLeft: 10, fontSize: 30, }}>Overview</Text>
                                </View>
                            </View>
                            <LineChart
                                style={s.line}
                                data={data}
                                width={Dimensions.get('window').width}
                                height={220}
                                chartConfig={{
                                    backgroundColor: '#e26a00',
                                    backgroundGradientFrom: '#77a3f9',
                                    backgroundGradientTo: '#51877c',
                                    decimalPlaces: 0, // optional, defaults to 2dp
                                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                    style: {
                                        borderRadius: 16
                                    }
                                }}
                            />
                            <Text style={{ fontWeight: 'bold', alignSelf: 'center' }}>My activity in last 7 days</Text>
                            {/* </TouchableOpacity> */}
                        </View>
                        {/* </Card> */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const s = StyleSheet.create({
    more: {
        justifyContent: 'center',
        alignSelf: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'red',
        width: 100,
        height: 30,
        borderRadius: 10
    },
    line: {
        marginVertical: 8,
        marginTop: 15,
        alignSelf: 'center',
        borderRadius: 16
    },
    title: {
        marginLeft: 10,
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white'
    }
})