import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Header } from 'react-native-elements';
import { createStackNavigator } from 'react-navigation';

export default class CustomDrawer extends Component {
    constructor(props) {
        super(props);
    }

    renderBK = () => {
        return (
            <View>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
            </View>
        )
    }

    render() {
        const menu = 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hamburger_icon.svg/220px-Hamburger_icon.svg.png';
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Header
                    leftComponent={this.renderBK()}
                    centerComponent={{ text: 'BKID', style: { color: 'white', fontSize: 20, fontWeight: 'bold' } }}
                    outerContainerStyles={{ backgroundColor: '#445870' }}
                />
                <TouchableOpacity style={s.item} onPress={() => { this.props.navigation.navigate('Home screen') }}>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Image style={{ width: 30, height: 30 }} source={require('./assets/icon_card.png')} />
                        <Text style={{ fontWeight: 'bold', marginLeft: 10, marginTop: 5 }}>HOME</Text>
                    </View>
                    <Image style={{ width: 30, height: 30 }} source={require('./assets/arrow_right.png')} />
                </TouchableOpacity>
                <TouchableOpacity style={s.item} onPress={() => { this.props.navigation.navigate('Transaction') }}>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Image style={{ width: 30, height: 30 }} source={require('./assets/history.png')} />
                        <Text style={{ fontWeight: 'bold', marginLeft: 10, marginTop: 5 }}>TRANSACTION</Text>
                    </View>
                    <Image style={{ width: 30, height: 30 }} source={require('./assets/arrow_right.png')} />
                </TouchableOpacity>
                <TouchableOpacity style={s.item} onPress={() => { this.props.navigation.navigate('subscribe') }}>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Image style={{ width: 30, height: 30 }} source={require('./assets/add-user.png')} />
                        <Text style={{ fontWeight: 'bold', marginLeft: 10, marginTop: 5 }}>SUBSCRIBED SERVICES</Text>
                    </View>
                    <Image style={{ width: 30, height: 30 }} source={require('./assets/arrow_right.png')} />
                </TouchableOpacity>
                <TouchableOpacity style={s.item} onPress={() => Alert.alert(
                    'Log out',
                    'Are you sure you want to log out?',
                    [
                        {
                            text: 'Cancel',
                            style: 'cancel',
                        },
                        { text: 'OK', onPress: () => this.props.navigation.navigate('Login') },
                    ],
                    { cancelable: false },
                )}>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Image style={{ width: 30, height: 30 }} source={require('./assets/logout_icon.png')} />
                        <Text style={{ fontWeight: 'bold', marginLeft: 10, marginTop: 5 }}>LOG OUT</Text>
                    </View>
                    <Image style={{ width: 30, height: 30 }} source={require('./assets/arrow_right.png')} />
                </TouchableOpacity>
            </View >
        );
    }
}

const s = StyleSheet.create({
    item: {
        flexDirection: 'row',
        marginTop: 10,
        marginVertical: 10,
        width: '100%',
        alignContent: 'center',
        justifyContent: 'space-between',
    },
});