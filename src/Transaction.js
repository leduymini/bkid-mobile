import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView, TouchableOpacity, Picker, Image, RefreshControl, Alert, FlatList } from 'react-native';
import { Header, Icon, Card, SearchBar } from 'react-native-elements';
import Axios from 'axios';
import Modal from "react-native-modal";
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class Transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            // data: [{ stationID: 'Parking B4', time: '18201820182018201820182018201820', card_UID: '1552064' }, { stationID: 'Parking B10', time: '743274327432743274327432', card_UID: '1552064' }, { stationID: 'Canteen', time: '14121412141214121412141214121412', card_UID: '1552064' }, { stationID: 'Lab 202C5', time: '083308330833083308330833', card_UID: '1552064' }],
            search: '',
            refreshing: false,
            modalVisible: false,
            sortValue: '',
            isModalVisible: false,
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            dates: '',
            times: '',
            student: [],
            loading: false,
        }
        this.arrayholder = [];
        this.options = [
            // { value: '', label: '...' },
            { value: 'A', label: 'A -> Z' },
            { value: 'Z', label: 'Z -> A' },
            { value: 'asc', label: 'Ascending' },
            { value: 'des', label: 'Descending' },
        ]
    }

    _showDatePicker = () => this.setState({ isDatePickerVisible: true });

    _showTimePicker = () => this.setState({ isTimePickerVisible: true });

    _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        let temp = date.toString();
        this.setState({ dates: temp.substring(4, 15) });
        this._hideDatePicker();
    };

    _handleTimePicked = (time) => {
        console.log('A date has been picked: ', time);
        let temp = time.toString();
        this.setState({ times: temp.substring(16, 21) });
        this._hideTimePicker();
    };

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    toggleModal(visible) {
        this.setState({ isModalVisible: visible });
    }

    searchFilterFunction = text => {
        const newData = this.arrayholder.filter(item => {
            const itemData = `${item.station_name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ data: newData, search: text });
    };

    onLoadData = () => {
        const student = this.props.navigation.getParam('api');
        this.setState({ refreshing: true })
        Axios.get("http://api-bkid.herokuapp.com/transactions/listtransaction/" + student.card_UID)
            .then((res) => {
                console.warn(res.data.docs);
                if (res.status == 200) {
                    let response = []
                    res.data.docs.map((item, index) => {
                        if (item.card_UID) {
                            response.push(item)
                        }
                    })
                    this.arrayholder = response;
                    // console.warn(this.arrayholder)
                    this.setState({ data: response, refreshing: false })
                    // this.setState({ refreshing: false })
                }
            })
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        fetchData().then(() => {
            this.setState({ refreshing: false });
        });
    }

    componentDidMount = () => {
        this.onLoadData()
    }

    onSorting = (itemValue, itemIndex) => {
        // let temp = this.state.data.time.toString();
        // let time = temp.substring(11, 19);
        this.setState({ sortValue: itemValue })
        if (this.state.sortValue === 'A') {
            this.setState({
                data: this.state.data.sort(function (a, b) {
                    var nameA = a.station_name.toUpperCase();
                    var nameB = b.station_name.toUpperCase();
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                })
            })
        } else if (this.state.sortValue === 'B') {
            this.setState({
                data: this.state.data.sort(function (b, a) {
                    var nameA = a.station_name.toUpperCase();
                    var nameB = b.station_name.toUpperCase();
                    if (nameB < nameA) {
                        return -1;
                    }
                    if (nameB > nameA) {
                        return 1;
                    }
                    return 0;
                })
            })
        } else if (this.state.sortValue === 'asc') {
            this.setState({
                data: this.state.data.sort(function (a, b) { return parseInt(new Date(a.time).toTimeString()) - parseInt(new Date(b.time).toTimeString()) })
            })
        } else if (this.state.sortValue === 'des') {
            this.setState({
                data: this.state.data.sort(function (a, b) { return parseInt(new Date(b.time).toTimeString()) - parseInt(new Date(a.time).toTimeString()) })
            })
        }

    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
                    <Icon
                        color='white'
                        name='menu' />
                </TouchableOpacity>
            </View>
        );
    }

    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 20, color: 'white' }}>TRANSACTION</Text>
            </View>
        );
    }

    renderRight = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Filter')}>
                    <Image style={{ width: 30, height: 30, tintColor: 'white' }} source={require('./assets/filter.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    renderItem = ({ item }) => {
        let day = new Date(item.time).toDateString();
        let time = new Date(item.time).toTimeString();
        // let time = items.substring(11, 19);
        // let temp = items.toDateString();
        return (
            // <TouchableOpacity onPress={() => { this.props.navigation.navigate('Detail', item.UID) }}>
            <Card style={{ width: '100', backgroundColor: 'white', flexDirection: 'column' }}>
                <Text>Station Name: {item.station_name}</Text>
                <Text>Date: {day}</Text>
                <Text>Time: {time}</Text>
            </Card>
            // </TouchableOpacity>
        )
    }

    render() {
        const { search } = this.state;
        const { data } = this.state;
        let datas = [
            { service: 'Station', values: [{ value: 'Lab 202C5' }, { value: 'Parking B4' }, { value: 'Parking B10' }] },
            { service: 'Service', values: [{ value: 'Check in' }, { value: 'Check out' }, { value: 'Canteen Food' }] },
        ];
        let picker;

        if (!data) {
            return (
                <View>
                    <Text>
                        Waiting for data
                    </Text>
                </View>
            )
        }
        else {
            return (
                <View style={{ flex: 1 }}>
                    <Header
                        leftComponent={this.renderMenu()}
                        centerComponent={this.renderCenter()}
                        outerContainerStyles={{ backgroundColor: '#445870' }}
                    />

                    <Modal isVisible={this.state.isModalVisible}
                        onBackButtonPress={() => { this.toggleModal(!this.state.isModalVisible); }}
                        onBackdropPress={() => { this.toggleModal(!this.state.isModalVisible); }}
                        animationIn={'slideInRight'}
                        animationOut={'slideOutRight'}
                        backdropOpacity={0.5}
                    >
                        <View style={s.modal}>
                            <View style={{ height: 40, justifyContent: 'center', backgroundColor: '#79eef7', alignItems: 'center' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Filter</Text>
                            </View>
                            <ScrollView
                            // refreshControl={
                            //     <RefreshControl
                            //         refreshing={this.state.refreshing}
                            //         onRefresh={this._onRefresh}
                            //     />
                            // }
                            >
                                {
                                    datas.map((item, index) => (
                                        <Dropdown
                                            key={index}
                                            label={item.service}
                                            data={item.values}
                                            containerStyle={{ marginLeft: 20, marginRight: 20, marginBottom: 20 }}
                                            dropdownPosition={-4}
                                        />
                                    ))
                                }
                                <TouchableOpacity onPress={this._showDatePicker} style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ marginLeft: 20, fontSize: 15 }}>Date</Text>
                                    <Icon
                                        containerStyle={{ marginRight: 20 }}
                                        color='gray'
                                        name='arrow-drop-down' />
                                </TouchableOpacity>
                                <DateTimePicker
                                    isVisible={this.state.isDatePickerVisible}
                                    onConfirm={this._handleDatePicked}
                                    onCancel={this._hideDatePicker}
                                />
                                <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginRight: 20, justifyContent: 'space-between' }}>
                                    <Text>{this.state.dates}</Text>
                                </View>

                                <View style={s.line} opacity={0.4} />

                                <TouchableOpacity onPress={this._showTimePicker} style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ marginLeft: 20, fontSize: 15 }}>Time</Text>
                                    <Icon
                                        containerStyle={{ marginRight: 20 }}
                                        color='gray'
                                        name='arrow-drop-down' />
                                </TouchableOpacity>
                                <DateTimePicker
                                    isVisible={this.state.isTimePickerVisible}
                                    onConfirm={this._handleTimePicked}
                                    onCancel={this._hideTimePicker}
                                    mode='time'
                                />
                                <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginRight: 20, }}>
                                    <Text style={{}}>{this.state.times}</Text>

                                </View>
                                <View style={s.line} opacity={0.4} />
                                <TouchableOpacity
                                    style={s.button}
                                    onPress={() => { this.toggleModal(!this.state.isModalVisible); }}
                                >
                                    <View>
                                        <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'white' }}>Apply</Text>
                                    </View>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
                    </Modal>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <SearchBar
                            placeholder="Type Here To Search"
                            onChangeText={text => this.searchFilterFunction(text)}
                            autoCorrect={false}
                            lightTheme
                            round
                            inputStyle={{ backgroundColor: 'white' }}
                            value={search}
                            containerStyle={{ width: '87%' }}
                        />
                        {/* <TouchableOpacity style={{ justifyContent: 'center', alignContent: 'center', }} onPress={() => this.props.navigation.navigate('Filter')}> */}
                        <TouchableOpacity style={{ justifyContent: 'center', alignContent: 'center', }} onPress={() => { this.toggleModal(!this.state.isModalVisible); }}>
                            <Icon
                                color='black'
                                reverse
                                containerStyle={{ height: 40, width: 40, backgroundColor: 'green' }}
                                name='format-list-bulleted' />
                        </TouchableOpacity>
                    </View>
                    {/* <TouchableOpacity */}
                    {/* onPress={() => { this.setModalVisible(!this.state.modalVisible); }}> */}
                    <View style={s.sort}>
                        <Text style={{ fontWeight: 'bold' }}>Sort: </Text>

                        {/* <Icon
                                color='black'
                                name='sort' /> */}
                        <View style={{ borderWidth: 0.2, borderRadius: 10 }}>
                            {/* <Picker
                                selectedValue={this.state.sortValue}
                                style={{ height: 30, width: 130, }}
                                onValueChange={(itemValue, itemIndex) =>{
                                    this.setState({sortValue: itemValue});
                                    this.onSorting(itemValue, itemIndex)
                                }}>
                                <Picker.Item label="..." value="" />
                                <Picker.Item label="A -> Z" value="A" />
                                <Picker.Item label="Z -> A" value="Z" />
                                <Picker.Item label="Ascending" value="asc" />
                                <Picker.Item label="Descending" value="des" />
                            </Picker> */}
                           <Dropdown
                                containerStyle={{ height: 30, marginLeft:6, width: 130, }}
                                inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                // rippleOpacity={0}
                                dropdownOffset={{top: 0,left:16}}
                                pickerStyle={{marginTop:50,width:130}}
                                dropdownPosition={0}
                                data={this.options}
                                onChangeText={(itemValue, itemIndex) =>{
                                    this.setState({sortValue: itemValue});
                                    this.onSorting(itemValue, itemIndex)
                                }}
                            />
                        </View>
                    </View>
                    {/* </TouchableOpacity> */}

                    {picker}

                    <FlatList
                        data={data}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item._id}
                        ListFooterComponent={<View style={{ marginBottom: 40, }} />}
                        ListEmptyComponent={
                            <View>
                                <Text style={{ textAlign: 'center' }}>There is no data</Text>
                            </View>
                        }
                        refreshing={this.state.refreshing}
                        onRefresh={this.onLoadData}
                    />
                </View>
            );
        }
    }
}

const s = StyleSheet.create({
    modal: {
        height: '90%',
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#edeeef'
    },
    sort: {
        marginLeft: 10,
        flexDirection: 'row',
        // width: 60,
        justifyContent: 'space-between',
        // borderLeftWidth: 0.2,
        // borderRightWidth: 0.2,
        // borderBottomWidth: 0.2,
        alignItems: 'center',
        alignSelf: 'center'
    },
    line: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        borderBottomColor: 'black',
        borderBottomWidth: 0.55,
        marginBottom: 10,
    },
    button: {
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: '#54a6ff',
        width: 150,
        height: 50,
        marginTop: 30,
        borderRadius: 10,
    },
})