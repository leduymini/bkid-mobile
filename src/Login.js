import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, BackHandler, StyleSheet, Alert, TextInput, TouchableOpacity } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { id: '', pass: '', spinner: false };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  onPressLogin = () => {
    this.setState({
      spinner: !this.state.spinner
    });
    const timer = setTimeout(() => {
      Alert.alert('Something wrong with the internet', 'Try it again or wait a minute :)');
      this.setState({
        spinner: false
      });
    }, 15000);

    axios.post('http://api-bkid.herokuapp.com/users/login', {
      studentID: this.state.id,
      password: this.state.pass
    })
      .then((response) => {
        if (response.data.name) {
          console.warn(response.data)
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Drawer', params: { api: response.data } })],
          });
          clearTimeout(timer);
          this.props.navigation.dispatch(resetAction);
        }
        else {
          this.setState({
            spinner: !this.state.spinner
          });
          clearTimeout(timer);
          alert('You have entered wrong id or password');
        }
        // alert(JSON.stringify(response));
      })
  }

  render() {
    return (
      <ImageBackground source={require('./assets/background.jpg')} style={{ width: '100%', height: '100%' }}>
        <Image source={require('./assets/Bk.png')} style={{ alignSelf: "center", width: 100, height: 100, marginTop: 50 }}></Image>
        <Text style={{ marginTop: 20, alignSelf: "center", fontWeight: "bold", fontSize: 30, color: 'white' }}>BKID</Text>
        <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center', alignContent: 'center' }}>
          {/* <Text style={{ marginLeft: 20, fontWeight: 'bold', fontSize: 20, alignSelf: 'center' }}>Student ID</Text> */}
          <TextInput
            style={{ height: 55, width: 250, paddingLeft: 10, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', backgroundColor: 'white', borderColor: 'gray', borderWidth: 1 }}
            onChangeText={(id) => this.setState({ id })}
            placeholderTextColor={'black'}
            placeholder="Student ID"
            value={this.state.id}
            returnKeyType={"next"}
            onSubmitEditing={() => { this.secondTextInput.focus(); }}
            blurOnSubmit={false}
          />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center', marginTop: 15 }}>
          {/* <Text style={{ marginLeft: 20, fontWeight: 'bold', fontSize: 20, alignSelf: 'center' }}>Password</Text> */}
          <TextInput
            secureTextEntry={true}
            placeholderTextColor={'black'}
            style={{ height: 55, width: 250, paddingLeft: 10, borderRadius: 10, alignSelf: 'center', backgroundColor: 'white', borderColor: 'gray', borderWidth: 1 }}
            onChangeText={(pass) => this.setState({ pass })}
            placeholder="Student password"
            value={this.state.pass}
            secureTextEntry={true}
            autoCorrect={false}
            ref={(input) => { this.secondTextInput = input; }}
            onSubmitEditing={() => { this.onPressLogin() }}
          />
        </View>
        <TouchableOpacity style={{ marginTop: 40 }} onPress={() => { this.onPressLogin() }}>
          <View style={{ width: 200, height: 50, backgroundColor: '#47525E', borderRadius: 10, alignContent: 'center', alignSelf: 'center', justifyContent: 'center' }}>
            <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fff', fontSize: 20 }}>Log In</Text>
          </View>
        </TouchableOpacity>
        <Spinner
          visible={this.state.spinner}
          // textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
          cancelable={true}
        />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
});
