import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ImageBackground, Dimensions, ScrollView } from 'react-native';
import { Header, Icon, Card } from 'react-native-elements';
import { createBottomTabNavigator } from 'react-navigation';
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            dates: '',
            times: '',
        };
    }

    _showDatePicker = () => this.setState({ isDatePickerVisible: true });

    _showTimePicker = () => this.setState({ isTimePickerVisible: true });

    _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        let temp = date.toString();
        this.setState({ dates: temp.substring(4, 15) });
        this._hideDatePicker();
    };

    _handleTimePicked = (time) => {
        console.log('A date has been picked: ', time);
        let temp = time.toString();
        this.setState({ times: temp.substring(16, 21) });
        this._hideTimePicker();
    };

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity hitSlop={{ bottom: 10,top: 10, left:10,right:10 }} onPress={() => { this.props.navigation.goBack() }}>
                    <Icon
                        color='white'
                        name='arrow-back' />
                </TouchableOpacity>
            </View>
        );
    }
    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 20, color: 'white' }}>FILTER</Text>
            </View>
        );
    }

    render() {
        let data = [
            { service: 'Station', values: [{ value: 'Lab 202C5' }, { value: 'Parking B4' }, { value: 'Parking B10' }] },
            { service: 'Service', values: [{ value: 'Check in' }, { value: 'Check out' }, { value: 'Canteen Food' }] },
        ];
        return (
            <View style={{ flex: 1 }}>
                <Header
                    leftComponent={this.renderMenu()}
                    centerComponent={this.renderCenter()}
                    outerContainerStyles={{ backgroundColor: '#445870' }}
                />
                <Text style={{ marginLeft: 20, marginTop: 20, fontWeight: 'bold', fontSize: 20 }}>Choose one or many to filter:</Text>
                <ScrollView>
                    {
                        data.map((item, index) => (
                            <Dropdown
                                key={index}
                                label={item.service}
                                data={item.values}
                                containerStyle={{ marginLeft: 20, marginRight: 20 }}
                                shadeOpacity={1.0}
                                rippleOpacity={1.0}
                            />
                        ))
                    }
                    <TouchableOpacity onPress={this._showDatePicker} hitSlop={{ bottom: 40 }}>
                        <Text style={{ marginLeft: 20, marginTop: 20, fontSize: 15 }}>Date</Text>
                    </TouchableOpacity>
                    <DateTimePicker
                        isVisible={this.state.isDatePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDatePicker}
                    />
                    <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginRight: 20, justifyContent: 'space-between' }}>
                        <Text style={{}}>{this.state.dates}</Text>
                        <Icon
                            color='gray'
                            name='arrow-drop-down' />
                    </View>

                    <View style={s.line} opacity={0.4} />

                    <TouchableOpacity onPress={this._showTimePicker} hitSlop={{ bottom: 40 }}>
                        <Text style={{ marginLeft: 20, marginTop: 20, fontSize: 15 }}>Time</Text>
                    </TouchableOpacity>
                    <DateTimePicker
                        isVisible={this.state.isTimePickerVisible}
                        onConfirm={this._handleTimePicked}
                        onCancel={this._hideTimePicker}
                        mode='time'
                    />
                    <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginRight: 20, justifyContent: 'space-between' }}>
                        <Text style={{}}>{this.state.times}</Text>
                        <Icon
                            color='gray'
                            name='arrow-drop-down' />
                    </View>
                    <View style={s.line} opacity={0.4} />
                    <TouchableOpacity style={s.button}>
                        <View>
                            <Text style={{fontWeight:'bold',fontSize:20,color:'white'}}>Apply</Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
}

const s = StyleSheet.create({
    line: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        borderBottomColor: 'black',
        borderBottomWidth: 0.55,
        marginBottom: 10,
    },
    button: {
        justifyContent: 'center', 
        alignSelf: 'center', 
        alignItems: 'center', 
        backgroundColor: 'gray', 
        width: 200, 
        height: 50,
        marginTop:30,
        borderRadius: 10,
    }
});