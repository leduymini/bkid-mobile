import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView, NavigationActions, StackActions } from 'react-navigation'
import Home from './Home'
import Transaction from './Transaction'
import CustomDrawer from './CustomDrawer'
import Onboarding from './Onboarding'
import Login from './Login'
import studentDetail from './studentDetail'
import subscribe from './subscribe'
import createStudent from './createStudent'
import filter from './filter'
import UserDetails from'./UserDetails'
import services from './services'

const AppMenu = createDrawerNavigator(
    {
        "Home screen": {
            screen: Home,
        },
        "Transaction": {
            screen: Transaction,
        },
        "subscribe": {
            screen: subscribe,
        },
        "Login": {
            screen: Login,
        },
    },
    {
        initialRouteName: "Home screen",
        contentComponent: CustomDrawer,
        drawerLockMode: 'locked-closed',
    },
)

// const prevGetStateForAction = Navigator.router.getStateForAction;

// Navigator.router.getStateForAction = (action, state) => {
//   // Do not allow to go back from Home
//   if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === 'Home') {
//     return null;
//   }

//   // Do not allow to go back to Login
//   if (action.type === 'Navigation/BACK' && state) {
//     const newRoutes = state.routes.filter(r => r.routeName !== 'Login');
//     const newIndex = newRoutes.length - 1;
//     return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
//   }
//   return prevGetStateForAction(action, state);
// };

const AppStack = createStackNavigator(
    {
        "Onboarding":{
            screen: Onboarding,
        },
        "Login":{
            screen: Login,
        },
        Drawer: {
            screen: AppMenu,
        },
        'Detail': {
            screen: studentDetail, 
        },
        "Home screen": {
            screen: Home,
        },
        'Create': {
            screen: createStudent
        },
        'Transaction': {
            screen: Transaction
        },
        "subscribe": {
            screen: subscribe,
        },
        "Filter":{
            screen: filter,
        },
        "User details":{
            screen: UserDetails,
        },
        "filter":{
            screen: filter,
        },
        "services":{
            screen: services,
        }
    },
    {
        initialRouteName: "Onboarding",
        navigationOptions: {
            header: null,
        }
    },
);

export default AppStack