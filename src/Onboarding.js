import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, ImageBackground } from 'react-native'
import Swiper from 'react-native-swiper'

export default class Onboarding extends Component {
  render() {
    return (
      <Swiper style={styles.wrapper} loop={false} ref={(ref) => { this.swiper = ref }}>

        <View style={styles.slide1}>
          <ImageBackground source={require('./assets/GreenGradient.png')} style={{ resizeMode: 'contain', width: '100%', height: '100%' }}>
            <Text style={{
              color: '#000',
              fontSize: 30,
              fontWeight: 'bold', marginLeft: 20,
              marginTop: 40
            }}>BKID</Text>
            <Text style={{ marginLeft: 20, color: '#000', marginTop: 10, fontSize: 15, fontStyle: 'italic' }}>Product of Duy, Anh and Trung</Text>
            <Image resizeMode="contain" style={{ marginTop: 30, alignSelf: 'center', width: 300, height: 300 }} source={require('./assets/student.png')} />
            <TouchableOpacity style={{justifyContent:'flex-end'}} onPress={() => this.swiper.scrollBy(5, true)}>
              <Text style={{ marginTop: 60, alignSelf: 'center' }}>Skip</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>

        <View style={styles.slide2}>
          <ImageBackground source={require('./assets/red_gradient.png')} style={{ resizeMode: 'contain', width: '100%', height: '100%' }}>
            <View style={{ alignSelf: 'center' }}>
              <Image source={require('./assets/Bk.png')} resizeMode="contain" style={{ marginTop: 100, alignSelf: 'center', width: 150, height: 150 }} />
              <Text style={styles.text}>Bach Khoa Identification</Text>
              <Text style={{ color: '#fff', alignSelf: 'center', marginTop: 10, fontSize: 15, fontWeight: 'bold' }}>An application will help you to manage daily life</Text>
              <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 15, fontWeight: 'bold' }}>in university enviroment</Text>
              <Text style={{ color: '#000', marginTop: 20, fontSize: 15, fontWeight: 'bold', fontStyle: 'italic', alignSelf: 'center' }}>So what does it have?</Text>
              <TouchableOpacity onPress={() => this.swiper.scrollBy(5, true)}>
                <Text style={{ marginTop: 20, alignSelf: 'center' }}>Skip</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>

        <View style={styles.slide3}>
          <ImageBackground source={require('./assets/blue_gradient.png')} style={{ resizeMode: 'contain', width: '100%', height: '100%' }}>
            <Image source={require('./assets/check_card.png')} resizeMode="contain" style={{ marginTop: 50, alignSelf: 'center', width: 200, height: 250 }} />
            <Text style={{ color: '#000',marginTop: 30, fontSize: 30, fontWeight: 'bold', alignSelf: 'center' }}>RFID</Text>
            <Text style={{ color: '#000',marginTop: 10, fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>Data transmitted via radio waves Technology</Text>
            <Text style={{ color: '#000',fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>It is Cheap, Convenient and Easy-to-use</Text>
            <TouchableOpacity onPress={() => this.swiper.scrollBy(5, true)}>
              <Text style={{ marginTop: 80, alignSelf: 'center' }}>Skip</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>

        <View style={styles.slide4}>
          <ImageBackground source={require('./assets/purple_gradient.jpg')} style={{ resizeMode: 'contain', width: '100%', height: '100%' }}>
            <Image source={require('./assets/All_card.png')} resizeMode="contain" style={{ marginTop: 50, alignSelf: 'center', width: 200, height: 250 }} />
            <Text style={{ color: '#000',marginTop: 30, fontSize: 30, fontWeight: 'bold', alignSelf: 'center' }}>All in one</Text>
            <Text style={{ color: '#000',marginTop: 10, fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>Student now dont have to carry many cards</Text>
            <Text style={{ color: '#000',fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>BKID learn function all of them</Text>
            <TouchableOpacity onPress={() => this.swiper.scrollBy(5, true)}>
              <Text style={{ marginTop: 80, alignSelf: 'center' }}>Skip</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>

        <View style={styles.slide5}>
          <ImageBackground source={require('./assets/orange_gradient.png')} style={{ resizeMode: 'contain', width: '100%', height: '100%' }}>
            <Image source={require('./assets/overview.png')} resizeMode="contain" style={{ marginTop: 50, alignSelf: 'center', width: 200, height: 250 }} />
            <Text style={{ color: '#000',marginTop: 30, fontSize: 30, fontWeight: 'bold', alignSelf: 'center' }}>Overview</Text>
            <Text style={{ color: '#000',marginTop: 10, fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>Charts will sum up all data every day</Text>
            <Text style={{ color: '#000',fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>Easy to track down any progress</Text>
            <TouchableOpacity onPress={() => this.swiper.scrollBy(5, true)}>
              <Text style={{ marginTop: 80, alignSelf: 'center' }}>Skip</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>

        <View style={{ backgroundColor: '#9DD6EB', flex: 1 }}>
          <Image source={require('./assets/school.jpg')} style={{ resizeMode: 'stretch', width: '100%', height: 300 }} />
          <Text style={{ alignSelf: 'center', fontWeight: 'bold', marginTop: 40, fontSize: 22 }}>Press below to sign in</Text>
          <Text style={{ marginTop: 5, alignSelf: 'center' }}>If you are Bach Khoa student</Text>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }} style={{ alignSelf: 'center', width: 200, height: 50, backgroundColor: '#47525E', borderRadius: 10, marginTop: 25 }}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontWeight: 'bold', color: '#fff' }}>Sign in</Text>
            </View>
          </TouchableOpacity>
        </View>
      </Swiper >
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  slide4: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  slide5: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#000',
    fontSize: 30,
    marginTop: 100,
    fontWeight: 'bold',
    alignSelf: 'center'
  }
})