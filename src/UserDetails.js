import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { Header, Icon, Card, Avatar } from 'react-native-elements';
import CreditCard from 'react-native-credit-card';
import Modal from "react-native-modal";

export default class info extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [{ name: 'Name', data: 'Le Duy' }, { name: 'Card Number', data: '12345-678' }, { name: 'Start Date', data: '09/15' }, { name: 'Student ID', data: '1552064' }],
            cons: ['Name', 'Card Number'],
            pla: [],
            lock: false,
            isModalVisible: false,
            card: false,
            email: 1,
            name:1,
            address:1,
            firm:1
        }
    }

    toggleSwitch = (value) => {
        //onValueChange of the switch this function will be called
        this.setState({ lock: value })
        //state changes according to switch
        //which will result in re-render the text
    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                    <Icon
                        color='white'
                        name='arrow-back' />
                </TouchableOpacity>
            </View>
        );
    }

    toggleModal(visible) {
        this.setState({ isModalVisible: visible });
    }

    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 20, color: 'white' }}>USER DETAILS</Text>
            </View>
        );
    }

    renderItem = ({ item }) => {
        return (
            <View>
                <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                    {/* <Text style={{fontWeight:'bold'}}>{item.name}</Text> */}
                    <Text style={{ marginLeft: 50 }}>{item.studentID}</Text>
                </View>
                <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>
            </View>
        )
    }

    expandEmail = () => {
        if (this.state.email == 1) {
            this.setState({
                email: 0,
            })
        }
        else
            this.setState({
                email: 1,
            })
    }

    expandAddress = () => {
        if (this.state.address == 1) {
            this.setState({
                address: 0,
            })
        }
        else
            this.setState({
                address: 1,
            })
    }

    expandFirm = () => {
        if (this.state.firm == 1) {
            this.setState({
                firm: 0,
            })
        }
        else
            this.setState({
                firm: 1,
            })
    }

    expandName = () => {
        if (this.state.name == 1) {
            this.setState({
                name: 0,
            })
        }
        else
            this.setState({
                name: 1,
            })
    }

    render() {
        const { data, cons, card } = this.state
        const student = this.props.navigation.getParam('student')
        const { pla } = this.state
        const date_of_birth = student.date_of_birth.substring(0, 10)
        const keys = ['name', 'studentID']
        const ho = keys.reduce((result, key) => ({ ...result, [key]: student[key] }), {})
        let content, top;
        if (card === false) {
            content =
                <ScrollView style={{ height: '50%' }}>
                    <View style={{ marginBottom: 100, marginTop: 30 }}>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginBottom: 10 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Name</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, fontSize: 18, marginRight: 20, }} onPress={() => this.expandName()} numberOfLines={this.state.name} ellipsizeMode="head">{student.name}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Student ID</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, fontSize: 18, marginRight: 20, }}>{student.studentID}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Gender</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, marginRight: 20, fontSize: 18 }}>{student.gender}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Phone number</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, marginRight: 20, fontSize: 18 }}>{student.phone}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Email</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, fontSize: 18, marginRight: 20, }} onPress={() => this.expandEmail()} numberOfLines={this.state.email} ellipsizeMode="middle">{student.email}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Address</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, marginRight: 20, fontSize: 18 }} onPress={() => this.expandAddress()} numberOfLines={this.state.address} ellipsizeMode="tail">{student.address}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Date of birth</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, marginRight: 20, fontSize: 18 }}>{date_of_birth}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, marginBottom: 10, justifyContent: 'space-around' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>School</Text>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'flex-start' }}>
                                <Text style={{ marginLeft: 50, marginRight: 20, fontSize: 18 }} onPress={() => this.expandFirm()} numberOfLines={this.state.firm} ellipsizeMode="tail">{student.firm}</Text>
                            </View>
                        </View>

                        <View style={{ marginLeft: 20, borderWidth: 0.5, marginRight: 20 }}></View>

                    </View>
                </ScrollView>

            top =
                <View style={{ alignSelf: 'center', marginTop: 30, justifyContent: 'center', alignItems: 'center', height: 150, width: 150, borderRadius: 150, backgroundColor: '#b0b4ba' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{student.name.substring(0, 1) + student.name.substring(3, 4)}</Text>
                </View>
        }
        else if (card === true) {
            content =
                <View style={{ marginTop: 20 }}>
                    <TouchableOpacity style={{ flexDirection: 'row', marginHorizontal: 5, marginBottom: 20 }}>
                        <Image source={require('./assets/lock.png')} style={{ width: 30, height: 30 }} resizeMode={'contain'} />
                        <Text style={{ fontSize: 20, marginLeft: 10, fontWeight: 'bold' }}>Lock card</Text>
                    </TouchableOpacity>

                    <View style={{ borderBottomWidth: 0.8 }} />

                    <TouchableOpacity style={{ flexDirection: 'row', marginHorizontal: 5, marginVertical: 20, }}>
                        <Image source={require('./assets/card_icon.png')} style={{ width: 30, height: 30 }} resizeMode={'contain'} />
                        <Text style={{ fontSize: 20, marginLeft: 10, fontWeight: 'bold' }}>Remake card</Text>
                    </TouchableOpacity>

                    <View style={{ borderBottomWidth: 0.8 }} />

                    <TouchableOpacity style={{ flexDirection: 'row', marginHorizontal: 5, marginVertical: 20, }}>
                        <Image source={require('./assets/password.png')} style={{ width: 30, height: 30 }} resizeMode={'contain'} />
                        <Text style={{ fontSize: 20, marginLeft: 10, fontWeight: 'bold' }}>Change password</Text>
                    </TouchableOpacity>

                    <View style={{ borderBottomWidth: 0.8 }} />
                </View >
            top =
                <ImageBackground style={{ alignSelf: 'center', marginTop: 30, height: 150, width: 250, justifyContent: 'flex-end' }} source={require('./assets/card-front.png')}>
                    <Text style={{ fontSize: 20, marginLeft: 5, color: '#fff' }}>{student.name}</Text>
                    <View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, justifyContent: 'space-between' }}>
                        <Text style={{ fontSize: 20, color: '#fff' }}>{student.card_number}</Text>
                        <Text style={{ fontSize: 20, color: '#fff' }}>{student.job}</Text>
                    </View>
                </ImageBackground>
        }

        return (
            <View>
                <Header
                    leftComponent={this.renderMenu()}
                    centerComponent={this.renderCenter()}
                    outerContainerStyles={{ backgroundColor: '#445870' }}
                />
                <View style={{ width: '100%', height: 250, backgroundColor: '#64FBF3', justifyContent: 'space-between' }}>
                    {/* <Modal isVisible={this.state.isModalVisible}
                        onBackButtonPress={() => { this.toggleModal(!this.state.isModalVisible); }}
                        onBackdropPress={() => { this.toggleModal(!this.state.isModalVisible); }}
                        animationIn={'slideInUp'}
                        animationOut={'slideOutDown'}
                        backdropOpacity={0.5}
                        style={{ backgroundColor: 'red' }}
                    >
                        <View style={{ alignItems: 'center', backgroundColor: 'blue', flex: 1 }}>
                            <ImageBackground resizeMode={'contain'} style={{ width: '80  %', height: '60%' }} source={require('./assets/card-front.png')}>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text>{student.phone}</Text>
                                </View>
                            </ImageBackground>
                        </View>
                    </Modal> */}

                    {top}
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                        <TouchableOpacity style={{ backgroundColor: '#D4B1B1', width: '50%', justifyContent: 'center', alignItems: 'center', borderWidth: 1 }} onPress={() => this.setState({ card: false })}>
                            <Text style={{ fontWeight: "bold", fontSize: 20 }}>Info</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ backgroundColor: '#F3F3F4', width: '50%', justifyContent: 'center', alignItems: 'center', borderWidth: 1 }} onPress={() => this.setState({ card: true })}>
                            <Text style={{ fontWeight: "bold", fontSize: 20 }}>Card</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                {content}

                {/* <FlatList
                    data={pla}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => item._id}
                    ListFooterComponent={<View style={{ marginBottom: 40, }} />}
                    ListEmptyComponent={
                        <View>
                            <Text style={{ textAlign: 'center' }}>There is no data</Text>
                        </View>
                    }
                /> */}

            </View>
        );
    }
}
